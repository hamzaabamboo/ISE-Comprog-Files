public class DiceStuff {
	public static final int faces = 100;
	public static final int times = 100000000;
	public static void main(String[] args) {
		int[] dat = new int[faces];
		for ( int i = 0 ; i < times; i++ ) dat[(int) Math.floor(Math.random()*(faces))]++;
		for ( int i = 0 ; i < faces; i++ ) System.out.println(toText(i+1) + " " + ((double) dat[i]*100)/times + "%");
	}
	public static String toNumber( int n ) {
		String[] nums = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten" };
		if ( n > 10 && n < 20) {
			switch ( n ) {
				case 11: return "eleven";
				case 12: return  "twelve";
				case 13: return "thirteen";
				case 14: return "fourteen";
				case 15: return "fifteen";
				default: return nums[(n%10)-1] + "teen";
			}
		}
		return (n >= 20) ? "" : nums[n-1];
	}
	public static String toText( int n ) {
		String res = "";
		String[] tens = { "twen", "thir", "four", "fif" , "six", "seven", "eigh", "nine"};
		if ( n >= 1000 ) {
			res += toNumber((int) n/1000 ) + " thousand ";
			if ( n % 1000 == 0 ) return res;
			else if ((int) (n % 1000)/100 == 0 ) res += "and ";
			res += toText( (int) (n % Math.pow(10, (int) Math.log10(n))));
		} else if ( n >= 100 ) {
			res += toNumber((int) n/100 ) + " hundred ";
			if ( n % 100 == 0 ) return res;
			else res += "and ";
			res += toText( (int) (n % Math.pow(10, (int) Math.log10(n))));
		}
		else {
			if ( n >= 20 ) {
				int ten = (n/10) - 2;
				res += (ten > 7 )? "" : ( tens[ten] + "ty ");
				if ( n % 10 == 0 ) return res;
				res += toNumber(n % 10);
			} else if ( n == 10 ) res = "ten";
			else {
				res += toNumber(n);
			}
		}
		return res;
	}
}
