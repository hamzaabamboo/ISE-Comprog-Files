import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
public class Fruits {
	String name;
	ArrayList<String> people;

	public Fruits(String name) {
		this.name = name;
		this.people = new ArrayList<String>();
	}
	public String toString(){
		String toPrint = "";
		toPrint += ("['" + this.name + "', [");
		for ( int j = 0 ; j < getSize() ; j++ ) 
			if ( j != getSize() - 1 ) toPrint += "'" + people.get(j) + "', ";
			else toPrint += "'" + people.get(j) + "'";
		toPrint += "]]";
		return toPrint;
	}

	public int getSize() { return this.people.size(); }
	public static void main(String[] args) throws IOException{
		LinkedHashMap<String, Fruits> fruits = new LinkedHashMap<String, Fruits>(); // Map containing data 
		Scanner kb = new Scanner(System.in); //Keyboard input
		Scanner s = new Scanner(new File(kb.nextLine()))
		;kb.close(); //File Scanner
		Fruits max = new Fruits("");
		while ( s.hasNextLine() ) {
			String[] data = s.nextLine().split(" ");
			if ( !fruits.containsKey(data[0]) ) {
				Fruits thisFruit = new Fruits(data[0]);
				thisFruit.people.add(data[1]);
				fruits.put(data[0], thisFruit);
			} else fruits.get(data[0]).people.add(data[1]);
		}
		s.close();
		// Print
		String toPrint = "[";
		for ( int i = 0 ; i < fruits.size(); i++ ){
			Fruits thisFruit = fruits.get(fruits.keySet().toArray()[i]);
			if ( thisFruit.getSize() > max.getSize() ) max = thisFruit;
			toPrint += thisFruit.toString();
			if ( i != fruits.size() - 1) toPrint += ", ";
		}
		toPrint += "]";
		System.out.println( toPrint + "\n" + "The most favorite fruit is " + max.name );
	}
}
