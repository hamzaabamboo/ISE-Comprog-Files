public class ISEStudent extends Student implements SpeakEnglish {
	String program;
	public ISEStudent(String name, int age, String stuid, String program) {
		super(name, age, stuid);
		this.program = program;
	}
	@Override
	public void speakEnglish() {	
		System.out.println("Sawasdee kub");
	}
	public String toString() {
		return name + ", " + age + ", " + program + ", " + stuid;
	}
}
