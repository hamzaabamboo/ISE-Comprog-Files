import java.util.Scanner;

public class MathPow {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Give me a and b ( with a space in between ) "); 
		double a = sc.nextDouble(), b = sc.nextDouble();
		sc.close();
		System.out.println((Math.pow(a,b)/(a*b)) + ((Math.pow(Math.E,a)*Math.abs(a-b))/(a+b))); 
		
	}
}
