import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Scanner;

public class ScoreLookup{
	public static void main(String [] args){
		String lookupString = constructLookUpStringFromFile("score.csv");
		boolean toQuit = false;
		do {
			char choice = showMainMenu();
			switch (choice) {
				case 'L':
					commenceLookUpProcedure(lookupString);
					break;
				case 'Q':
					toQuit = true;
					break;
				default:
					System.out.println("Invalid choice. Quitting.");
					toQuit = true;
			}
		} while(!toQuit);
	}
	
	//Add static methods here.
	// The methods include, but not limited to
	// - constructLookUpStringFromFile(String)
	// - showMainMenu()
	// - commenceLookUpProcedure(String)
	public static String constructLookUpStringFromFile(String path) {
		Scanner s;
		String res = "";
		try {
			URL url = ScoreLookup.class.getClassLoader().getResource(path);
			s = new Scanner(new File(url.getPath()));
			s.nextLine();
			while ( s.hasNext() ) res += s.nextLine() + "\n";
			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return res;
	}
	public static char showMainMenu() {
		Scanner k = new Scanner(System.in);
		System.out.println("---------------------------------\nMain Menu\n---------------------------------");
		System.out.println("L )\tLook up score \nQ )\tQuit\n---------------------------------");
		System.out.print(">> ");
		String in = k.nextLine();
		return ( in.length() == 1 ? in.charAt(0) : 'z' );
	}
	public static void commenceLookUpProcedure(String lookupString) {
		Scanner s = new Scanner(lookupString);
		Scanner kb = new Scanner(System.in);
		System.out.print("Enter Student ID: ");
		String id = kb.nextLine();
		while( s.hasNextLine() ) {
			String curLine = s.nextLine();
			if ( curLine.substring(0,10).equals(id) ) {
				String[] dat = curLine.split(",");
				int sum = 0;
				for ( String score : dat ) if ( score.length() <= 2 ) sum += Integer.parseInt(score);
				System.out.println("\t\tQ1\tQ2\tQ3\tQ4\tQ5\tTotal");
				System.out.printf("%s\t%s\t%s\t%s\t%s\t%s\t%d\n", dat[0], dat[1], dat[2], dat[3], dat[4], dat[5], sum);
				s.close();
				return;
			}
		}
		System.out.println("Score not found !");
	}
}