public class Student extends Person {
	String stuid;
	public Student(String name, int age, String stuid) {
		super(name, age);
		this.stuid = stuid;
	}
	public String toString() {
		return name + ", " + age + ", " + stuid;
	}
}
