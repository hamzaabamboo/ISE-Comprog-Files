public class TypeChecker  {
	public static String primName(Object o) {
		String ty = o.getClass().getSimpleName().toString();
		switch ( ty ) {
			case "Integer": return "int";
			case "Character": return "char";
			case "Long": return "long";
			case "Double": return "double";
			case "Boolean": return "boolean";
			case "Float": return "float";
			default: return ty;
		}
	}
	public static void main(String[] args) {
        Object[] o = { 256, 4.0, 4.0F, 'A', 256L, 0xAA, 256D, -1e10, true };
        for ( int i = 0 ; i < o.length; i++ ) System.out.println(primName(o[i]));
	}		
}